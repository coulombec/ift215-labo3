/**
 * @File: SettingPanel.java
 * @Date : 2014-09-30
 */
package labo3;

/**
 * IFT215 Labo3 Code de base du SettingPanel.
 *
 * @author Charles Coulombe
 */
public class SettingPanel extends javax.swing.JPanel {

    /**
     * Creates new form SettingPanel
     */
    public SettingPanel() {
        initComponents();
    }

    private void initComponents() {

        textPanel1 = new labo3.TextPanel();
        _pnlMaths = new labo3.MathPanel();
        _pnlTexte = new labo3.TextPanel();
        _pnlBtns = new javax.swing.JPanel();
        _btnRenitialiser = new javax.swing.JButton();
        _btnQuitter = new javax.swing.JButton();

        setPreferredSize(new java.awt.Dimension(330, 400));

        _pnlMaths.setName("_pnlMaths"); // NOI18N

        _pnlTexte.setName("_pnlTexte"); // NOI18N

        _btnRenitialiser.setText("Quitter");

        _btnQuitter.setText("Réinitialiser");

        javax.swing.GroupLayout _pnlBtnsLayout = new javax.swing.GroupLayout(_pnlBtns);
        _pnlBtns.setLayout(_pnlBtnsLayout);
        _pnlBtnsLayout.setHorizontalGroup(
                _pnlBtnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(_pnlBtnsLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(_btnQuitter)
                        .addGap(6, 6, 6)
                        .addComponent(_btnRenitialiser, javax.swing.GroupLayout.PREFERRED_SIZE, 87, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
        );
        _pnlBtnsLayout.setVerticalGroup(
                _pnlBtnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(_pnlBtnsLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(_pnlBtnsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(_btnQuitter)
                                .addComponent(_btnRenitialiser))
                        .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                                .addComponent(_pnlMaths, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(_pnlTexte, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(_pnlBtns, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(24, 24, 24))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(_pnlTexte, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(_pnlMaths, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(_pnlBtns, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(41, Short.MAX_VALUE))
        );
    }

    // Variables declaration - do not modify
    private javax.swing.JButton _btnQuitter;
    private javax.swing.JButton _btnRenitialiser;
    private javax.swing.JPanel _pnlBtns;
    private labo3.MathPanel _pnlMaths;
    private labo3.TextPanel _pnlTexte;
    private labo3.TextPanel textPanel1;
    // End of variables declaration
}
