/**
 * @File : TextPanel.java
 * @Date : 2014-09-30
 */
package labo3;

/**
 * IFT215 Labo3 Code de base du TextPanel.
 *
 * @author Charles Coulombe
 */
public class TextPanel extends javax.swing.JPanel {

    /**
     * Creates new form TextPanel
     */
    public TextPanel() {
        initComponents();
    }

    private void initComponents() {

        _lblEntrezTexte = new javax.swing.JLabel();
        _btnItalique = new javax.swing.JButton();
        _btnGras = new javax.swing.JButton();
        _btnCouleur = new javax.swing.JButton();
        _txtTexte = new javax.swing.JTextField();

        setBorder(javax.swing.BorderFactory.createTitledBorder("Traitement de texte"));
        setName("_pnlText"); // NOI18N

        _lblEntrezTexte.setText("Entrez votre texte :");

        _btnItalique.setText("Italique");

        _btnGras.setText("Gras");

        _btnCouleur.setText("Couleur");

        _txtTexte.setText("ici");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addComponent(_btnItalique, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(_btnGras, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(_btnCouleur, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGroup(layout.createSequentialGroup()
                                        .addComponent(_lblEntrezTexte)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(_txtTexte)))
                        .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(_lblEntrezTexte)
                                .addComponent(_txtTexte, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(_btnItalique)
                                .addComponent(_btnGras)
                                .addComponent(_btnCouleur)))
        );
    }

    // Variables declaration - do not modify
    private javax.swing.JButton _btnCouleur;
    private javax.swing.JButton _btnGras;
    private javax.swing.JButton _btnItalique;
    private javax.swing.JLabel _lblEntrezTexte;
    private javax.swing.JTextField _txtTexte;
    // End of variables declaration
}
