/**
 * @File : MathPanel.java
 * @Date : 2014-09-30
 */
package labo3;

/**
 * IFT215 Labo3 Code de base du MathPanel.
 *
 * @author Charles Coulombe
 */
public class MathPanel extends javax.swing.JPanel {

    /**
     * Creates new form MathPanel
     */
    public MathPanel() {
        initComponents();
    }

    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        _lblEntrezNb = new javax.swing.JLabel();
        _txtNombre = new javax.swing.JTextField();
        _lblInc = new javax.swing.JLabel();
        _btnInc = new javax.swing.JButton();
        _lblDec = new javax.swing.JLabel();
        _btnDec = new javax.swing.JButton();
        rbMul = new javax.swing.JRadioButton();
        _rbAjout = new javax.swing.JRadioButton();
        _cbValeur = new javax.swing.JComboBox();
        _lblResultatEst = new javax.swing.JLabel();
        _lblResultat = new javax.swing.JLabel();

        setBorder(javax.swing.BorderFactory.createTitledBorder("Calculs simples"));
        setName("_pnlMaths"); // NOI18N
        setPreferredSize(new java.awt.Dimension(267, 212));

        _lblEntrezNb.setText("Entrez un nombre :");

        _txtNombre.setPreferredSize(new java.awt.Dimension(43, 23));

        _lblInc.setText("Incrémenter :");

        _btnInc.setText("+1");
        _btnInc.setPreferredSize(new java.awt.Dimension(43, 23));

        _lblDec.setText("Décrémenter : ");

        _btnDec.setText("-1");

        rbMul.setText("Multiplier par");

        _rbAjout.setText("Ajouter");

        _cbValeur.setModel(new javax.swing.DefaultComboBoxModel(new String[]{"Item 1", "Item 2", "Item 3", "Item 4"}));

        _lblResultatEst.setText("Le résultat est :");

        _lblResultat.setText("42");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(_cbValeur, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                        .addComponent(_lblInc, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(_lblEntrezNb, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                                .addComponent(_lblDec)
                                                .addComponent(rbMul)
                                                .addComponent(_lblResultatEst))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                .addComponent(_rbAjout)
                                                .addComponent(_txtNombre, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(_lblResultat, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(_btnInc, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                                                .addComponent(_btnDec, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[]{_lblDec, _lblEntrezNb, _lblInc});

        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(_lblEntrezNb)
                                .addComponent(_txtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(_lblInc)
                                .addComponent(_btnInc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(_lblDec)
                                .addComponent(_btnDec))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(rbMul)
                                .addComponent(_rbAjout))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(_cbValeur, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(_lblResultatEst)
                                .addComponent(_lblResultat)))
        );
    }

    // Variables declaration - do not modify
    private javax.swing.JButton _btnDec;
    private javax.swing.JButton _btnInc;
    private javax.swing.JComboBox _cbValeur;
    private javax.swing.JLabel _lblDec;
    private javax.swing.JLabel _lblEntrezNb;
    private javax.swing.JLabel _lblInc;
    private javax.swing.JLabel _lblResultat;
    private javax.swing.JLabel _lblResultatEst;
    private javax.swing.JRadioButton _rbAjout;
    private javax.swing.JTextField _txtNombre;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JRadioButton rbMul;
    // End of variables declaration
}
