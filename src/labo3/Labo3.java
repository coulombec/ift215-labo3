/**
 * @File : Labo3.java
 * @Date : 2014-09-30
 */
package labo3;

import javax.swing.JFrame;

/**
 * IFT215 Labo3 Code de base.
 *
 * @author Charles Coulombe
 */
public class Labo3 {

    /**
     * Main program entry point.
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //1. Création du JFrame
        JFrame frame = new JFrame("Labo3");

        //2. Optionnel: On quitte lors de la fermeture du JFrame
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //3. Création du panneau et l'ajoute au JFrame
        frame.add(new SettingPanel());

        //4. Assigne les dimensions.
        frame.pack();

        //5. On affiche le tout.
        frame.setVisible(true);
    }

}
